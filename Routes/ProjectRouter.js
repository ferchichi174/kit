// ProjectRouter.js
const express = require('express');
const router = express.Router();
const ProjectController = require('../Controllers/ProjectController');

//Projects
router.get('/', ProjectController.getAllProjects);
router.get('/:id', ProjectController.getProjectById);

module.exports = router;


