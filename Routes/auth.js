const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../Models/UserModel'); // Assurez-vous que le chemin d'accès est correct
require('dotenv').config();
const router = express.Router();

// Inscription
router.post('/register', async (req, res) => {
    const { username, password, email } = req.body; // Assurez-vous d'inclure 'email' si votre schéma l'exige
    try {
        const newUser = new User({
            username,
            password, // stocké en clair
            email
        });
        await newUser.save();
        res.status(201).send('User created successfully');
    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });
    }
});


router.post('/login', async (req, res) => {
    const { username, password } = req.body;
    try {
        const user = await User.findOne({ username });
        if (user && password === user.password) { // comparaison directe
            const token = jwt.sign({ userId: user._id }, 'your-secret-key', { expiresIn: '1h' });
            res.json({ token });
        } else {
            res.status(400).json({ error: 'Invalid credentials' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Internal server error' });
    }
});

module.exports = router;
