// TaskRouter.js
const express = require('express');
const router = express.Router();
const TaskController = require('../Controllers/TaskController');

router.get('/', TaskController.getAllTasks);

router.get('/:id', TaskController.getTaskById);


module.exports = router;


