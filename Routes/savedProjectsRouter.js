const express = require('express');
const router = express.Router();
const savedProjectsController = require('../Controllers/SavedProjectsController');

router.get('/getProject', savedProjectsController.getProjects);

module.exports = router;


