const express = require('express');
const axios = require('axios');

const app = express();
const PORT = 5000;

app.use(express.json());

app.use(cors());


app.get('/api/v1/tasks/projects/:projectID', async (req, res) => {
    try {
        // Fetch tasks data from the provided JSON URL
        const tasksResponse = await axios.get('https://middleware-clickup.onrender.com/api/v1/tasks');

        const projectID = req.params.projectID;
        console.log('Requested Project ID:', projectID);
  
        // Filter tasks based on the provided projectID
        const tasks = ta
        tasksResponse.data.filter(task => { console.log('Task Folder: ', task.folder) ; return task.folder === projectID; });
  
        // Log filtered tasks
        console.log('Filtered tasks:', tasks);
  
        // Return the filtered tasks
        res.json(tasks);
    } catch (error) {
        console.error('Error fetching tasks:', error);
        res.status(500).json({ error: 'Internal server error' });
    }
  });


  app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});