// ProjectModel.js
const mongoose = require("mongoose")
const Schema = mongoose.Schema

const projectSchema = new Schema({
    id: String,
    name: String,
    estimatedStartDate: Date,
    estimatedEndDate: Date,
    description: String,
    tasks: [{
        type: Schema.Types.ObjectId,
        ref: 'Task'
    }],
    users: [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }]

}) 

const Project = mongoose.model("Project", projectSchema)

module.exports = Project;

