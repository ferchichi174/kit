const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const taskSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    projectId: { type: String, required: true },  // Maintenant, projectId est une chaîne

    description: String,
    dueDate: Date,
    status: {
        type: String,
        enum: ['pending', 'ongoing', 'completed'],
        default: 'pending'
    }
});

const Task = mongoose.model("Task", taskSchema);

module.exports = Task;
