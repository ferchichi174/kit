// TaskController.js
const TaskService = require('../Services/TaskService');


// *********  getAppTasks  ******************

exports.getAllTasks = async (req, res) => {
    try {
        const tasks = await TaskService.getAllTasks();
        res.json(tasks);
    } catch (error) {
        console.error('Error fetching tasks:', error);
        res.status(500).json({ error: 'Failed to fetch tasks' });
    }
};

// *********  getTaskById  ******************
exports.getTaskById = async (req, res) => {
    try {
        const taskId = req.params.id;
        const task = await TaskService.getTaskById(taskId);
        if (!task) {
            return res.status(404).json({ error: 'Task not found' });
        }
        res.json(task);

    } catch (error) {
        console.error('Error fetching task by ID:', error);
        res.status(500).json({ error: 'Failed to fetch task' });
    }
};




