const Project = require('../Services/SavedProjectsService');

const getProjects = async (req, res) => {
    try {
        const projects = await Project.findProjects();
        res.status(201).json(projects);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
};


module.exports = {
    getProjects, 
};





