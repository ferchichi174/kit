// ProjectController.js
const ProjectService = require('../Services/ProjectService');
const Project = require('../Models/ProjectModel');



// *********  getAppProjects  ******************

exports.getAllProjects = async (req, res) => {
    try {
        const projects = await ProjectService.getAllProjects();
        res.json(projects);
    } catch (error) {
        console.error('Error fetching projects:', error);
        res.status(500).json({ error: 'Failed to fetch projects' });
    }
};


// *********  getProjectById  ******************
exports.getProjectById = async (req, res) => {
    try {
        const projectId = req.params.id;
        const project = await ProjectService.getProjectById(projectId);
        if (!project) {
            return res.status(404).json({ error: 'Project not found' });
        }
        res.json(project);

    } catch (error) {
        console.error('Error fetching project by ID:', error);
        res.status(500).json({ error: 'Failed to fetch project' });
    }
};
