const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const axios = require('axios');
const authRouter = require('./Routes/auth');
const bcrypt = require('bcrypt');

const Project = require("./Models/ProjectModel");
const projectRouter = require('./Routes/ProjectRouter');
const taskRouter = require('./Routes/TaskRouter');
const userRouter = require('./Routes/UserRouter');

const savedProjectsRouter = require('./Routes/savedProjectsRouter');

const { MongoClient, ServerApiVersion } = require('mongodb');
const Task = require("./Models/TaskModel");
const uri = "mongodb+srv://eyarejeb02:vVJ1xZf3CFG79Cya@kpiproject.ysb2uow.mongodb.net/?retryWrites=true&w=majority&appName=KPIProject";

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

// const newTask = new Task({
//   name: 'Develop new feature11',
//   // projectId: '663051cf7067993bacd179c5', // Assurez-vous que ceci est l'ObjectId d'un projet existant dans votre DB
//   description: 'Develop a new feature for our project management tool.',
//   dueDate: new Date('2024-12-31'),
//   status: 'pending'
// });
//
// newTask.save()
//     .then(task => console.log('New Task Created:', task))
//     .catch(err => console.error('Error creating new task:', err));

// const existingUserId = '663526eaa2bc974768b7858f';
// const existingTaskId = '66366b0aead5f96e6ef8e626';
//
// // Create a new project and link it to an existing user and task
// const createProject = new Project({
//   name: 'New Project',
//   estimatedStartDate: new Date(),
//   estimatedEndDate: new Date(),
//   description: 'Project Description',
//   users: [existingUserId],  // Linking existing user to the project
//   tasks: [existingTaskId]   // Linking existing task to the project
// });

// createProject.save()
//     .then(project => {
//       console.log('Project created with linked user and task:', project);
//     })
//     .catch(err => console.error('Error creating project:', err));
async function run() {
  try {
    // Connect the client to the server	(optional starting in v4.7)
    await client.connect();
    // Send a ping to confirm a successful connection
    await client.db("admin").command({ ping: 1 });
    console.log("Pinged your deployment. You successfully connected to MongoDB!");
  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
run().catch(console.dir);


const app = express();
const PORT = 5000;

// Middleware pour analyser les données JSON du corps de la requête
app.use(express.json());

// Middleware pour autoriser les requêtes cross-origin
app.use(cors());

// Connexion à la base de données MongoDB
mongoose.connect("mongodb+srv://eyarejeb02:vVJ1xZf3CFG79Cya@kpiproject.ysb2uow.mongodb.net/test?retryWrites=true&w=majority&appName=KPIProject")
    .then(() => {
      console.log("Connected Successfully!!!");
    })
    .catch((error) => {
      console.log("Error connecting with the database", error);
    });

// Route pour récupérer tous les projets from database
app.use('/api/projects', savedProjectsRouter);

// auth route
app.use('/auth', authRouter);


app.get('/api/tasks/projects/:projectId', async (req, res) => {
  try {
    const { projectId } = req.params;
    // Trouver toutes les tâches qui ont un projectId spécifique
    const tasks = await Task.find({ projectId: projectId });
    console.error('tasks:', tasks);
    if (!tasks.length) {
      return res.status(404).json({ message: 'No tasks found for this project' });
    }

    res.json(tasks);
  } catch (error) {
    console.error('Error fetching tasks:', error);
    res.status(500).json({ error: 'Failed to fetch tasks' });
  }
});
app.post('/api/tasks/addTask', async (req, res) => {
  try {
    const { name, projectId, description, dueDate, status } = req.body;

    // Création d'une nouvelle tâche
    const newTask = new Task({
      name,       // Nom de la tâche
      projectId,  // ID du projet associé
      description,// Description de la tâche
      dueDate,    // Date d'échéance
      status      // Statut de la tâche, e.g., 'pending'
    });

    // Sauvegarde de la nouvelle tâche dans la base de données
    const savedTask = await newTask.save();

    // Réponse avec la tâche créée
    res.status(201).json(savedTask);
  } catch (error) {
    console.error('Error adding task:', error);
    res.status(400).json({ error: 'Failed to add task', details: error });
  }
});


app.get('/api/projects/:projectId/users', async (req, res) => {
  const { projectId } = req.params;

  try {
    const users = await User.find({ projectId: projectId });
    if (users.length === 0) {
      return res.status(404).json({ message: 'No users found for this project' });
    }

    res.json(users);
  } catch (error) {
    console.error('Error fetching users:', error);
    res.status(500).json({ error: 'Failed to fetch users', details: error });
  }
});
// Route pour ajouter un nouveau projet

app.post('/api/projects/addProject', async (req, res) => {
  const projectData = req.body;

  const project = new Project(projectData);
  try {
    const newProject = await project.save();
    res.status(201).json(newProject); // Retourne l'objet complet du nouveau projet
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});
app.get('/api/projects/:projectId', async (req, res) => {
  try {
    const project = await Project.findById(req.params.projectId);
    if (!project) {
      return res.status(404).json({ message: 'Project not found' });
    }
    res.json(project);
  } catch (error) {
    console.error('Error fetching project:', error);
    res.status(500).json({ error: 'Failed to fetch project' });
  }
});

// DELETE project
app.delete('/api/projects/deleteProject/:id', async (req, res) => {
  try {
    const deletedProject = await Project.findByIdAndDelete(req.params.id);
    if (!deletedProject) {
      return res.status(404).json({ error: 'Project not found' });
    }
    res.json({ message: 'Project deleted successfully' });
  } catch (error) {
    console.error('Error deleting project:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});


app.use('/api/projects', projectRouter);
app.use('/api/tasks', taskRouter);
app.use('/api/users', userRouter);

// Custom Endpoints

// tasks related endpoints
// Endpoint to get list of tasks by project ID
app.get('/api/tasks/projects/:projectId', async (req, res) => {
  try {
    const { projectId } = req.params;

    // Fetch tasks data from the provided URL
    const response = await axios.get('https://middleware-clickup.onrender.com/api/v1/folders');
    const projects = response.data;

    // Find the project with the matching ID
    const project = projects.find(proj => proj.id === projectId);

    if (!project) {
      console.error('Project not found');
      return res.status(404).json({ error: 'Project not found' });
    }

    // Extract list of users from the project
    const users = project.space.members.map(member => ({
      id: member.user.id,
      username: member.user.username,
      color: member.user.color,
      profilePicture: member.user.profilePicture,
      initials: member.user.initials
    }));

    console.log('Users:', users);
    res.json(users);

  } catch (error) {
    console.error('Error fetching tasks:', error);
    res.status(500).json({ error: 'Failed to fetch tasks' });
  }
});

// Endpoint to get a single task by task ID
app.get('/api/tasks/projects/:projectId', async (req, res) => {
  const { projectId } = req.params;

  try {
    const tasks = await Task.find({ projectId: projectId });
    if (tasks.length === 0) {
      return res.status(404).json({ message: 'No tasks found for this project' });
    }

    res.json(tasks);
  } catch (error) {
    console.error('Error fetching tasks:', error);
    res.status(500).json({ error: 'Failed to fetch tasks' });
  }
});
// Endpoint to get list of tasks by userId
app.get('/api/tasks/users/:userId', async (req, res) => {
  try {
    const { userId } = req.params;

    // Fetch tasks data from the provided URL
    const response = await axios.get('https://middleware-clickup.onrender.com/api/v1/tasks');
    const tasks = response.data;

    // Filter tasks by userId
    const filteredTasks = tasks.filter(task => {
      try {
        // Check if assignees array exists and is not empty
        if (task.assignees && task.assignees.length > 0) {
          // Check if any of its elements contain the userId
          return task.assignees.some(assignee => assignee.id === userId);
        } else {
          // Return false if assignees array is empty
          return false;
        }
      } catch (error) {
        // Handle the error here, such as logging it or returning a default value
        console.error("Error occurred while filtering tasks:", error);
        // Return false or any other default value to exclude the task from the filtered array
        return false;
      }
    });


    res.json(filteredTasks);

  } catch (error) {
    console.error('Error fetching users:', error);
    res.status(500).json({ error: 'Failed to fetch users' });
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
