// UserService.js
const axios = require('axios');

exports.getAllUsers = async () => {
    try {
        const response = await axios.get('https://middleware-clickup.onrender.com/api/v1/folders');
        return response.data;
    } catch (error) {
        throw error;
    }
};


exports.getUserById = async (UserId) => {
    try {
        const response = await axios.get(`https://middleware-clickup.onrender.com/api/v1/folders/${UserId}`);
        return response.data;
    } catch (error) {
        throw error;
    }
};




