// TaskService.js
const axios = require('axios');

exports.getAllTasks = async () => {
    try {
        const response = await axios.get('https://middleware-clickup.onrender.com/api/v1/tasks');
        return response.data;
    } catch (error) {
        throw error;
    }
};


exports.getTaskById = async (taskId) => {
    try {
        const response = await axios.get(`https://middleware-clickup.onrender.com/api/v1/tasks/${taskId}`);
        return response.data;
    } catch (error) {
        throw error;
    }
};




