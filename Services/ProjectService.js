// ProjectService.js
const axios = require('axios');

exports.getAllProjects = async () => {
    try {
        const response = await axios.get('https://middleware-clickup.onrender.com/api/v1/folders');
        return response.data;
    } catch (error) {
        throw error;
    }
};


exports.getProjectById = async (projectId) => {
    try {
        const response = await axios.get(`https://middleware-clickup.onrender.com/api/v1/folders/${projectId}`);
        return response.data;
    } catch (error) {
        throw error;
    }
};




